const express = require ('express');
const app = express ();
require ('dotenv').config ();
const cors = require ('cors');

let roomList = [
  {
    id: 1,
    name: 'Hotel A',
    city: 'Helsinki',
    address: 'Green Street 12-14, 00010 Helsinki',
    type: 'Hotel',
    rooms: [
      {type: '1 single bed', quantity: 1, price: 40},
      {type: '1 double bed', quantity: 1, price: 60},
      {type: '2 single beds', quantity: 1, price: 50},
      {type: '3 single beds', quantity: 0, price: 50},
    ],
  },
  {
    id: 2,
    name: 'AirBnb B',
    city: 'Helsinki',
    address: 'Green Street 18, 00010 Helsinki',
    type: 'AirBnb',
    rooms: [
      {type: '1 single room', quantity: 1, price: 40},
      {type: '4 beds', quantity: 1, price: 32},
      {type: '8 beds', quantity: 2, price: 26},
      {type: '12 beds', quantity: 1, price: 20},
    ],
  },
  {
    id: 3,
    name: 'AirBnb C',
    city: 'Vaasa',
    address: 'Palosaarentie 102, 65300 Vaasa',
    type: 'AirBnb',
    rooms: [
      {type: '1 single room', quantity: 0, price: 40},
      {type: '4 beds', quantity: 0, price: 32},
      {type: '8 beds', quantity: 2, price: 26},
      {type: '12 beds', quantity: 1, price: 20},
    ],
  },
];

app.use (cors ());
app.use (express.json ());
app.use (express.static ('build'));

app.get ('/available', (req, res) => {
  res.status (200).json (roomList);
});

app.get ('/available/:id', (req, res) => {
  const id = Number (req.params.id);
  const room = roomList.find (obj => obj.id === id);
  if (room) {
    res.json (room);
  } else {
    res.status (404).end ();
  }
});

app.post ('/new', (req, res) => {
  const body = req.body;

  const id = roomList.length > 0
    ? Math.max (...roomList.map (n => Number (n.id)))
    : 0;

  if (!body) {
    return res.status (400).json ({
      error: 'Fill all info!',
    });
  }

  const newRoom = {
    id: id + 1,
    name: body.name,
    type: body.type,
    city: body.city,
    address: body.address,
    rooms: body.rooms,
  };

  roomList = roomList.concat (newRoom);

  res.status (201).json (newRoom);
});

const port = process.env.PORT || 3001;
app.listen (port, () => {
  console.log (`Server running on port ${port}`);
});
